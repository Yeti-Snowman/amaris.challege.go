package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

type ApiResponse struct {
	Global    *Global          `json:"global,ommitempty"`
	Countries []CountrySummary `json:"countries,ommitempty"`
	Date      string           `json:"date,ommitempty"`
}
type Global struct {
	NewConfirmed   int64 `json:"newConfirmed,ommitempty"`
	TotalConfirmed int64 `json:"totalConfirmed,ommitempty"`
	NewDeaths      int64 `json:"newDeaths,ommitempty"`
	TotalDeaths    int64 `json:"totalDeaths,ommitempty"`
	NewRecovered   int64 `json:"newRecovered,ommitempty"`
	TotalRecovered int64 `json:"totalRecovered,ommitempty"`
}
type CountrySummary struct {
	Country        string `json:"country,ommitempty"`
	CountryCode    string `json:"countryCode,ommitempty"`
	Slug           string `json:"slug,ommitempty"`
	NewConfirmed   int64  `json:"newConfirmed,ommitempty"`
	TotalConfirmed int64  `json:"totalConfirmed,ommitempty"`
	NewDeaths      int64  `json:"newDeaths,ommitempty"`
	TotalDeaths    int64  `json:"totalDeaths,ommitempty"`
	NewRecovered   int64  `json:"newRecovered,ommitempty"`
	TotalRecovered int64  `json:"totalRecovered,ommitempty"`
	Date           string `json:"date,ommitempty"`
}

type Data struct {
	Country      string `json:"country,ommitempty"`
	Iso          string `json:"countryCode,ommitempty"`
	Confirmed    int64  `json:"confirmed,ommitempty"`
	NewConfirmed int64  `json:"newConfirmed,ommitempty"`
	Deaths       int64  `json:"deaths,ommitempty"`
}

type Response struct {
	Error string `json:"error,ommitempty"`
	Data  *Data  `json:"data,ommitempty"`
}

var apiResponse ApiResponse

func covidSummary(w http.ResponseWriter, req *http.Request) {
	fmt.Println("Endpoint Hit: covidSummary")
	params := mux.Vars(req)

	var response Response

	for _, item := range apiResponse.Countries {
		if item.CountryCode == strings.ToUpper(params["countryId"]) {
			response.Data = getData(item)
			json.NewEncoder(w).Encode(response)
			return
		}
	}
	json.NewEncoder(w).Encode(&Response{})
}
func getData(item CountrySummary) *Data {
	var response Data
	response.Confirmed = item.TotalConfirmed
	response.Country = item.Country
	response.Deaths = item.TotalDeaths
	response.Iso = item.CountryCode
	response.NewConfirmed = item.NewConfirmed
	return &response
}
func home(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "Welcome to the Amaris Challege!")
}

func handleRequests() {
	router := mux.NewRouter()

	router.HandleFunc("/", home).Methods("GET")
	router.HandleFunc("/covid-summary/{countryId}", covidSummary).Methods("GET")

	log.Fatal(http.ListenAndServe(":10000", router))
}

func loadCovidSumary() {
	fmt.Println("Starting the application...")
	res, err := http.Get("https://api.covid19api.com/summary")
	if err != nil {
		panic(err.Error())
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err.Error())
	}

	s, err := getCountrySummary([]byte(body))
	apiResponse.Countries = s.Countries
}

func getCountrySummary(body []byte) (*ApiResponse, error) {
	var s = new(ApiResponse)
	err := json.Unmarshal(body, &s)
	if err != nil {
		fmt.Println("wtf:", err)
	}
	return s, err
}
func main() {
	fmt.Println("Rest API v1.0 - Amaris Challege")
	loadCovidSumary()
	handleRequests()
}
